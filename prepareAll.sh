#!/bin/sh

set -euo pipefail

rm -f *.tar.gz

VERSION="`wget http://www.iptvplayer.gitlab.io/update2/lastversion.php -q -O -`"
./prepareInstallPackage.sh 2.7 ${VERSION}_python2.7.tar.gz
./prepareInstallPackage.sh 2.6 ${VERSION}_python2.6.tar.gz
./prepareInstallPackage.sh X.X ${VERSION}_pythonX.X.tar.gz
echo ""
echo "#####################################################################"
echo "#   Install packages based on the version $VERSION are ready   #"
echo "#####################################################################"
